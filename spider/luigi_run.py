
import luigi
from run import main


class TestTask1(luigi.Task):
    task_num = luigi.IntParameter()

    def run(self):
        main(self.task_num, self)


class TestTask2(luigi.Task):
    task_num = luigi.IntParameter()

    def run(self):
        main(self.task_num, self)


if __name__ == '__main__':
    # luigi.run()
    luigi.build([TestTask1(task_num = 1), TestTask2(task_num = 2)], workers = 2)