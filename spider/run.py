
import os
import time
import logging
import requests
import json
import multiprocessing as mp

from scrapper.publisher import Publisher
from scrapper.scrapperfacade import ScrapperFacade
from scrapper.ads.ad import Ad


def get_logger(task_num):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to info
    # handler = logging.StreamHandler()
    if task_num == 1:
        handler = logging.FileHandler('spider1.log', 'a+')
    else:
        handler = logging.FileHandler('spider2.log', 'a+')
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        fmt = '%(asctime)s %(levelname)-8s %(message)s', 
        datefmt = '%Y-%m-%d %H:%M:%S'
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def main(task_num, task_obj):
    print ('\nIn main task %s\n' %task_num)

    get_logger(task_num)

    publisher_obj = Publisher()
    publisher_json = publisher_obj.getPublisher()

    # initialize start time
    start_time = time.time()

    scraper_obj = ScrapperFacade(publisher_json, task_obj)
    scraper_obj.scrape_operation()

    # initialize stop time
    stop_time = time.time()

    logging.info('Total time to scrape: %s' %(stop_time - start_time))
    # print (json.dumps(scraper_obj.ads))

    if scraper_obj.ads:
        ad = Ad(scraper_obj.ads)        
    else:
        patch_publisher = {
            'publisher_id': publisher_json['pi'],
            'has_ads': 0
        }

    time.sleep(3)
    os.system('sudo rm -rf /tmp/*')


if __name__ == '__main__':
    p = mp.Process(target=main)
    p.start()
    p.join()
