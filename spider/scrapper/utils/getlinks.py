
import re
import random
from random import shuffle
from bs4 import BeautifulSoup

from scrapper.utils.getcrawlinglinks import get_crawling_links


def getlinks(self, task_obj, url):
    self.soup = BeautifulSoup(task_obj.driver.page_source, "html.parser")

    # get crawling links to look for ads in that current page
    links, hpage = get_crawling_links(self, task_obj, url)

    link_list = []
    link_count = 0
    link_random_total = random.randint(4,20)

    for tag in links:
        try:
            link = tag.get('href', None)
        except:
            link = tag
        link = link.rstrip('/')
        
        # and link_count <= link_random_total
        if link is not None and link != self.publisher_url and link not in link_list and link != hpage:
            if not re.search(r'(contact|terms|privacy|about-us|facebook|twitter|subscribe|subscription|mailto|login|signin|feed|cookie|page|policy|currPage|settings|sitemap|about|submit)', link):
                link_list.append(link)
                link_count += 1

    # inplace list shuffling
    shuffle(link_list)

    return link_list