
import re


def get_crawling_links(self, task_obj, url):
    if 'teastart' in url:
        hpage = 'http://teastart.com'
        links = self.soup.find_all('a', href = re.compile('^' + hpage))
    elif 'smartcompany' in url:
        hpage = url
        links = list(map(lambda elem: elem.get_attribute('href'), task_obj.driver.find_elements_by_xpath('//h2[@class="entry-title"]/a')))
    elif 'aviationnews' in url:
        hpage = url
        links = list(map(lambda elem: elem.get_attribute('href'), task_obj.driver.find_elements_by_xpath('//a[contains(@href, "/?categoryId=111634")]')))
    else:
        hpage = url
        # links = self.soup.find_all('a', href = re.compile('^' + self.publisher_url))
        if 'http://' in url:
            word = url.replace('www', '').split('http://')[1].split('.')[0]
        elif 'https://' in url:
            word = url.replace('www', '').split('https://')[1].split('.')[0]
        links = self.soup.find_all('a', href=re.compile(word))
    
    links = list(set(links))

    return links, hpage