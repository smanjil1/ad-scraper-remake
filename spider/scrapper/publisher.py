
import logging
from scrapper import config
import requests
from requests.auth import HTTPBasicAuth
import json
import urllib3

urllib3.disable_warnings()


class Publisher(object):
    cnc_url = config.CNC_PUB_URL

    def __init__(self):
        # self.publisher_json = {"url": "http://www.90skidsonly.com", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 774, "device_id": 1}

        # self.publisher_json = {"url": "http://teastart.com/author/sharon", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

        # self.publisher_json = {"url": "https://www.smartcompany.com.au/business-advice", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}
        
        # self.publisher_json = {"url": "http://www.thepostgame.com/", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}
        
        # self.publisher_json = {"url": "https://www.cbssports.com/", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

        # self.publisher_json = {"url": "http://www.aviationnews.technology/?categoryId=111634", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

        self.publisher_json = {"url": "http://puppytoob.com", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

        # self.publisher_json = {"url": "http://memolition.com", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}
        
        # self.publisher_json = {"url": "http://www.fadedindustry.com", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

        # self.publisher_json = {"url": "http://bigtimedollars.com/", "country_id": 2, "proxy_port": 1727, "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36", "country_code": "US", "publisher_id": 77847, "device_id": 1}

    def getPublisher(self):
        # logging.info('Publisher: %s' %self.publisher_json)
        # return self.publisher_json

        request = requests.request(
            method = 'get', 
            url = Publisher.cnc_url
            # auth = HTTPBasicAuth(config.CNC_USERNAME, config.CNC_PASSWORD)
        )

        return request.json()
