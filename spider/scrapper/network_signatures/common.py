
import logging
import os
import time
import uuid
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey


def landing_page_save(scrapper_obj):
    lp_uuid = str(uuid.uuid4())

    landing_page = {}
    landing_page['lp_upload_folder'] = 'lp'
    landing_page['lp_screenshot_file'] = lp_uuid + '.png'
    
    # save html dump of landing page
    lp_html_filename = lp_uuid + '.html'
    html_element = scrapper_obj.task_obj.driver.find_element_by_tag_name('html').get_attribute('innerHTML')
    f = open('%s' % lp_html_filename, 'w+')
    f.write(html_element)
    # os.rename(lp_html_filename, '/tmp/%s' % lp_html_filename)
    os.rename(lp_html_filename, Spidey.workdir + 'lp_html/%s' % lp_html_filename)

    landing_page['lp_html_file'] = lp_html_filename

    # get screenshot
    lp_url = scrapper_obj.task_obj.driver.current_url

    scrapper_obj.task_obj.driver.quit()
    logging.info('Firefox closed!!!!')

    # os.system('sudo kill $(pidof firefox)')
    # time.sleep(3)
    
    profile_name = str(uuid.uuid4())
    os.system('/usr/bin/firefox -CreateProfile %s' % profile_name)
    os.system('/usr/bin/firefox -p %s -screenshot %s %s' % (profile_name, Spidey.workdir + 'scrs/' + landing_page['lp_screenshot_file'], lp_url))
    logging.info('Screenshot captured!!!!')
    
    scrapper_obj.init_firefox(scrapper_obj.task_obj)

    return landing_page
