
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save


class ContentAd(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('ContentAd....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('contentad', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['ContentAd']
        contentad_path_identifier = 'ac_container'

        # get links with contentad path identifier
        contentad_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//div[contains(@class, "%s")]' % contentad_path_identifier)

        contentad_links = list(map(lambda elem: elem.find_element_by_xpath('a').get_attribute('href'), contentad_elements))
        contentad_titles = list(map(lambda elem: elem.find_element_by_xpath('a/img').get_attribute('title'), contentad_elements))
        contentad_images = list(map(lambda elem: elem.find_element_by_xpath('a/img').get_attribute('src'), contentad_elements))
        
        if contentad_elements:
            self.scrapper_obj.total_ads_to_scrape += len(contentad_elements)

            for i, element in enumerate(contentad_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id
                ad['ad_image_src'] = contentad_images[i]
                ad['ad_line_1'] = contentad_titles[i]

                try:
                    # escape overlays
                    actions = ActionChains(self.scrapper_obj.task_obj.driver)
                    actions.send_keys(Keys.ESCAPE)
                    actions.perform()

                    self.scrapper_obj.task_obj.driver.get(contentad_links[i])
                except Exception as e:
                    logging.info(str(e.args))

                try:
                    logging.info('Landing Page Enter....')
                    
                    ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                    landing_page_dict = landing_page_save(self.scrapper_obj)

                    ad.update(landing_page_dict)

                    self.scrapper_obj.ads[ad_id] = ad
                except Exception as e:
                    logging.info(str(e.args))

                self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no contentad ads present!')