
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save
    

class MgLine(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('Mgline....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('mgline', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['mgid']
        mgline_path_identifier = 'mgline '

        # get links with mgline path identifier
        mgline_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//div[contains(@class, "%s")]/div[@class="image-with-text"]' %mgline_path_identifier)

        mgline_links = list(map(lambda elem: elem.find_element_by_xpath('div[@class="text-elements"]/div[@class="text_on_hover"]/div[@class="mctitle"]/a').\
                                get_attribute('href'), mgline_elements))
        mgline_titles = list(map(lambda elem: elem.find_element_by_xpath('div[@class="text-elements"]/div[@class="text_on_hover"]/div[@class="mctitle"]/a').text, mgline_elements))
        mgline_innerHTML = list(map(lambda elem: elem.get_attribute('innerHTML'), mgline_elements))
        
        if mgline_elements:
            self.scrapper_obj.total_ads_to_scrape += len(mgline_elements)

            for i, element in enumerate(mgline_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id
                ad['ad_line_1'] = mgline_titles[i]
                
                inner_html = mgline_innerHTML[i]

                inner_html_souped = BeautifulSoup(inner_html, "html.parser")

                img_rc_photo_element = inner_html_souped.find('img', class_="mcimg")

                if img_rc_photo_element:                
                    ad['ad_image_src'] = img_rc_photo_element.get('src')

                    try:
                        # escape overlays
                        actions = ActionChains(self.scrapper_obj.task_obj.driver)
                        actions.send_keys(Keys.ESCAPE)
                        actions.perform()
                        
                        self.scrapper_obj.task_obj.driver.get(mgline_links[i])
                    except Exception as e:
                        logging.info(str(e.args))

                    try:
                        logging.info('Landing Page Enter....')
                        
                        ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                        landing_page_dict = landing_page_save(self.scrapper_obj)

                        ad.update(landing_page_dict)

                        self.scrapper_obj.ads[ad_id] = ad
                    except Exception as e:
                        logging.info(str(e.args))

                    self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no mgline ads present!')