
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save


class AdBlade(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('Adblade....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('adblade', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['AdBlade']
        adblade_path_identifier = 'ad r-'

        # get links with adblade path identifier
        adblade_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//div[contains(@class, "%s")]' % adblade_path_identifier)

        adblade_links = list(map(lambda elem: elem.find_element_by_xpath('div[@class="description"]/a').get_attribute('href'), adblade_elements))
        adblade_titles = list(map(lambda elem: elem.find_element_by_xpath('div[@class="description"]/a').text, adblade_elements))
        adblade_images = list(map(lambda elem: elem.find_element_by_xpath('div[@class="img"]/a/img').get_attribute('src'), adblade_elements))
        
        if adblade_elements:
            self.scrapper_obj.total_ads_to_scrape += len(adblade_elements)

            for i, element in enumerate(adblade_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id      
                ad['ad_image_src'] = adblade_images[i]
                ad['ad_line_1'] = adblade_titles[i]

                try:
                    actions = ActionChains(self.scrapper_obj.task_obj.driver)
                    actions.send_keys(Keys.ESCAPE)
                    actions.perform()
                    
                    self.scrapper_obj.task_obj.driver.get(adblade_links[i])
                except Exception as e:
                    logging.info(str(e.args))

                try:
                    logging.info('Landing Page Enter....')
                    
                    ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                    landing_page_dict = landing_page_save(self.scrapper_obj)

                    ad.update(landing_page_dict)

                    self.scrapper_obj.ads[ad_id] = ad
                except Exception as e:
                    logging.info(str(e.args))
                
                self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no adblade ads present!')