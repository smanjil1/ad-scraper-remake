
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save


class OutBrain(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('Outbrain....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('outbrain', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['OutBrain']
        outbrain_path_identifier = 'ob-dynamic-rec-link '

        # get links with outbrain path identifier
        outbrain_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//a[contains(@class, "%s")]' % outbrain_path_identifier)

        outbrain_links = list(map(lambda elem: elem.get_attribute('href'), outbrain_elements))
        outbrain_innerHTML = list(map(lambda elem: elem.get_attribute('innerHTML'), outbrain_elements))
        
        if outbrain_elements:
            self.scrapper_obj.total_ads_to_scrape += len(outbrain_elements)

            for i, element in enumerate(outbrain_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id
                
                inner_html = outbrain_innerHTML[i]

                inner_html_souped = BeautifulSoup(inner_html, "html.parser")

                img_rc_photo_element = inner_html_souped.find('img', class_="ob-rec-image ob-show")

                if img_rc_photo_element:                
                    ad['ad_image_src'] = img_rc_photo_element.get('src')
                    ad['ad_line_1'] = img_rc_photo_element.get('title')

                    try:
                        # escape overlays
                        actions = ActionChains(self.scrapper_obj.task_obj.driver)
                        actions.send_keys(Keys.ESCAPE)
                        actions.perform()

                        self.scrapper_obj.task_obj.driver.get(outbrain_links[i])
                    except Exception as e:
                        logging.info(str(e.args))

                    try:
                        logging.info('Landing Page Enter....')
                        
                        ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                        landing_page_dict = landing_page_save(self.scrapper_obj)

                        ad.update(landing_page_dict)

                        self.scrapper_obj.ads[ad_id] = ad
                    except Exception as e:
                        logging.info(str(e.args))

                    self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no outbrain ads present!')
