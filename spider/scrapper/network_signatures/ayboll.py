
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save


class AyBoll(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('AyBoll....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('ayboll', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['AyBoll']
        ayboll_path_identifier = 'ayboll-tb'

        # get links with ayboll path identifier
        ayboll_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//div[contains(@class, "%s")]' % ayboll_path_identifier)

        ayboll_links = list(map(lambda elem: elem.find_element_by_xpath('a').get_attribute('href'), ayboll_elements))
        ayboll_titles = list(map(lambda elem: elem.find_element_by_xpath('a').get_attribute('title'), ayboll_elements))
        ayboll_images = list(map(lambda elem: elem.find_element_by_xpath('a').get_attribute('innerHTML'), ayboll_elements))
        
        if ayboll_elements:
            self.scrapper_obj.total_ads_to_scrape += len(ayboll_elements)

            for i, element in enumerate(ayboll_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id      
                ad['ad_line_1'] = ayboll_titles[i]

                inner_html_souped = BeautifulSoup(ayboll_images[i], "html.parser")
                img_element = inner_html_souped.find('span', class_="ayboll-tbb")
                ad['ad_image_src'] = ''.join(['http://', img_element.attrs['style'].split('url(//')[1].split(');')[0]])

                try:
                    actions = ActionChains(self.scrapper_obj.task_obj.driver)
                    actions.send_keys(Keys.ESCAPE)
                    actions.perform()
                    
                    self.scrapper_obj.task_obj.driver.get(ayboll_links[i])
                except Exception as e:
                    logging.info(str(e.args))

                try:
                    logging.info('Landing Page Enter....')
                    
                    ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                    landing_page_dict = landing_page_save(self.scrapper_obj)

                    ad.update(landing_page_dict)

                    self.scrapper_obj.ads[ad_id] = ad
                except Exception as e:
                    logging.info(str(e.args))

                self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no ayboll ads present!')
