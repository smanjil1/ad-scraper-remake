
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save


class RevContent(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('RevContent....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('revcontent', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['RevContent']
        revcontent_path_identifier = 'trends.revcontent.com'

        # get links with revcontent in it
        revcontent_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//a[contains(@href, "%s")]' % revcontent_path_identifier)

        revcontent_titles = list(map(lambda elem: elem.get_attribute('title'), revcontent_elements))
        revcontent_links = list(map(lambda elem: elem.get_attribute('href'), revcontent_elements))
        revcontent_innerHTML = list(map(lambda elem: elem.get_attribute('innerHTML'), revcontent_elements))
        
        if revcontent_elements:
            self.scrapper_obj.total_ads_to_scrape += len(revcontent_elements)

            for i, element in enumerate(revcontent_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id

                ad['ad_line_1'] = revcontent_titles[i]
                inner_html = revcontent_innerHTML[i]

                inner_html_souped = BeautifulSoup(inner_html, "html.parser")

                div_rc_photo_element = inner_html_souped.find('div', class_="rc-photo")

                if div_rc_photo_element:
                    revcontent_ad_image_div = div_rc_photo_element.attrs['style']
                    revcontent_ad_image_url = ''.join(['http://', revcontent_ad_image_div.split('url("//')[1].split('");')[0]])
                    
                    ad['ad_image_src'] = revcontent_ad_image_url

                    try:
                        # escape overlays
                        actions = ActionChains(self.scrapper_obj.task_obj.driver)
                        actions.send_keys(Keys.ESCAPE)
                        actions.perform()

                        self.scrapper_obj.task_obj.driver.get(revcontent_links[i])
                    except Exception as e:
                        logging.info(str(e.args))

                    try:
                        logging.info('Landing Page Enter....')
                        
                        ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                        landing_page_dict = landing_page_save(self.scrapper_obj)

                        ad.update(landing_page_dict)

                        self.scrapper_obj.ads[ad_id] = ad
                    except Exception as e:
                        logging.info(str(e.args))

                    self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no revcontent ads present!')
