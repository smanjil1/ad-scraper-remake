
import logging
import time
import random
import re
import uuid
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from scrapper.spidey import Spidey
from scrapper.network_signatures.common import landing_page_save
    

class Taboola(object):
    def __init__(self, scrapper_obj):
        self.scrapper_obj = scrapper_obj

    def start_scrape(self, link):
        logging.info('Taboola....')
        self.scrapper_obj.total_network_sigs_scraped += 1

        logging.info('Looking for %s ads in: %s' % ('taboola', link))
        start = time.time()
        self.scrapper_obj.task_obj.driver.get(link)
        logging.info('Received response from pub: %s in %s seconds' %(link, time.time() - start))
        time.sleep(random.randint(4, 20))

        network_id = self.scrapper_obj.networks['Taboola']
        taboola_path_identifier = 'videoCube trc_spotlight_item '

        '''
            some taboola networks sigs page have infinite scrolling.
            So, to get a reasonable number of ads scrolled down throughout the page for 5 times.
            And, then the taboola ads are extracted.
        '''
        for i in range(5):
            self.scrapper_obj.task_obj.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(5)

        taboola_elements = self.scrapper_obj.task_obj.driver.find_elements_by_xpath('//div[contains(@class, "%s")]' %taboola_path_identifier)

        taboola_titles = list(map(lambda elem: elem.find_element_by_xpath('a[@class=" item-thumbnail-href "]').get_attribute('title'), taboola_elements))
        taboola_links = list(map(lambda elem: elem.find_element_by_xpath('a[@class=" item-thumbnail-href "]').get_attribute('href'), taboola_elements))
        taboola_innerHTML = list(map(lambda elem: elem.find_element_by_xpath('a[@class=" item-thumbnail-href "]').get_attribute('innerHTML'), taboola_elements))
        
        if taboola_elements:
            self.scrapper_obj.total_ads_to_scrape += len(taboola_elements)

            for i, element in enumerate(taboola_elements):
                ad = {}
                ad_id = str(uuid.uuid4())

                ad['network_id'] = str(network_id)
                ad['publisher_id'] = self.scrapper_obj.publisher_id
                ad['device_id'] = self.scrapper_obj.device_id
                ad['country_id'] = self.scrapper_obj.country_id
                ad['ad_line_1'] = taboola_titles[i]
                
                inner_html = taboola_innerHTML[i]

                inner_html_souped = BeautifulSoup(inner_html, "html.parser")

                img_rc_photo_element = inner_html_souped.find('span', class_="thumbBlock")

                if img_rc_photo_element:                
                    ad['ad_image_src'] = img_rc_photo_element.get('style').split('url("')[1].split('");')[0]

                    try:
                        # escape overlays
                        actions = ActionChains(self.scrapper_obj.task_obj.driver)
                        actions.send_keys(Keys.ESCAPE)
                        actions.perform()
                        
                        self.scrapper_obj.task_obj.driver.get(taboola_links[i])
                    except Exception as e:
                        logging.info(str(e.args))

                    try:
                        logging.info('Landing Page Enter....')
                        
                        ad['lp_url'] = self.scrapper_obj.task_obj.driver.current_url

                        landing_page_dict = landing_page_save(self.scrapper_obj)

                        ad.update(landing_page_dict)

                        self.scrapper_obj.ads[ad_id] = ad
                    except Exception as e:
                        logging.info(str(e.args))

                    self.scrapper_obj.total_ads_scraped += 1
        else:
            logging.info('Oops! no taboola ads present!')