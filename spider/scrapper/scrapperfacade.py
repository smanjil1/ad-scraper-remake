
import logging
import os
import random
import re
import time
from random import shuffle

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException

from scrapper.network_signatures.adblade import AdBlade
from scrapper.network_signatures.ayboll import AyBoll
from scrapper.network_signatures.contentad import ContentAd
from scrapper.network_signatures.mgline import MgLine
from scrapper.network_signatures.outbrain import OutBrain
from scrapper.network_signatures.revcontent import RevContent
from scrapper.network_signatures.taboola import Taboola
from scrapper.network_signatures.zergnet import Zergnet
from scrapper.utils.getlinks import getlinks

from .spidey import Spidey

os.environ['MOZ_HEADLESS'] = '1'


class ScrapperFacade(Spidey):
    def __init__(self, publisher_dict, task_obj):
        self.task_obj = task_obj
        self.publisher_dict = publisher_dict
        self.proxy = Spidey.proxy + str(self.publisher_dict['pp'])
        self.user_agent = self.publisher_dict['ua']
        self.publisher_id = self.publisher_dict['pi']
        self.device_id = self.publisher_dict['di']
        self.country_id = self.publisher_dict['ci']
        self.publisher_url = self.publisher_dict['u']
        self.publisher_domain = self.publisher_url.replace('www.', '')
        self.networks = {
            "Taboola": 1,       # done
            "OutBrain": 2,      # done
            "RevContent": 3,    # done
            "ContentAd": 4,     # done
            "AdBlade": 5,       # done
            "mgid": 7,          # done
            "YahooGemini": 8, 
            "AyBoll": 9,        # done
            "Zergnet": 12       # done
        }
        
        self.ads = {}

        self.total_ads_to_scrape = 0
        self.total_ads_scraped = 0
        self.total_network_sigs_scraped = 0

        # initialize network sigs class
        self.ayboll = AyBoll(self)
        self.adblade = AdBlade(self)
        self.contentad = ContentAd(self)
        self.mgline = MgLine(self)
        self.outbrain = OutBrain(self)
        self.revcontent = RevContent(self)
        self.taboola = Taboola(self)
        self.zergnet = Zergnet(self)

    def init_firefox(self, task_obj = None):
        if task_obj:
            self.task_obj = task_obj

        try:
            # self.driver = webdriver.Firefox()
            self.task_obj.driver = webdriver.Firefox()
            self.task_obj.driver.set_page_load_timeout(3600)
            logging.info('Firefox driver started!')
        except Exception as e:
            logging.info('Timeout: %s' %str(e.args))
            logging.info('Retrying....')
            # self.driver = webdriver.Firefox()
            self.task_obj.driver = webdriver.Firefox()
            self.task_obj.driver.set_page_load_timeout(3600)
            logging.info('Firefox driver started!')

    def close_driver(self):
        # self.driver.quit()
        self.task_obj.driver.quit()

        logging.info('Firefox driver closed!')

        logging.info('Total ads to scrape: %s' %self.total_ads_to_scrape)
        logging.info('Total ads scraped: %s' %self.total_ads_scraped)
        logging.info('Total network signatures scraped: %s' %self.total_network_sigs_scraped)

    def scrape_operation(self):
        if 'http' not in self.publisher_url:
            self.publisher_url = ''.join(['http://', self.publisher_dict['url']])
            
        # init driver
        self.init_firefox()

        start = time.time()
        # get page
        self.task_obj.driver.get(self.publisher_url)
        logging.info('Received response from pub %s' %self.publisher_url)
        logging.info('Time to get the response from pub without using proxy: %s', time.time() - start)

        self.soup = BeautifulSoup(self.task_obj.driver.page_source, "html.parser")

        # get crawling links to look for ads in that current page
        link_list = getlinks(self, self.task_obj, self.publisher_url)

        # inplace list shuffling
        shuffle(link_list)

        logging.info('Total Links: %s' %len(link_list))

        for i, link in enumerate(link_list):
            if i == 0:
                # link = 'http://puppytoob.com/dog-news/organization-is-turning-throw-away-dogs-into-k-9-heroes/'
                try:
                    logging.info('Initializing network signatures....')
                    time.sleep(7)

                    # call scrapping job
                    self.ayboll.start_scrape(link)
                    self.adblade.start_scrape(link)
                    self.contentad.start_scrape(link)
                    self.mgline.start_scrape(link)
                    self.outbrain.start_scrape(link)
                    self.revcontent.start_scrape(link)
                    self.taboola.start_scrape(link)
                    self.zergnet.start_scrape(link)
                except Exception as e:
                    logging.info(str(e.args))

        # close driver
        # self.close_driver()
