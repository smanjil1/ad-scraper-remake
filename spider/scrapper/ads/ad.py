
import logging
import os
import json
import hashlib
import random
import uuid
import requests
import boto3
from PIL import Image

from scrapper.spidey import Spidey
from scrapper import config


class Ad(Spidey):
    cnc_url = config.CNC_API_URL

    def __init__(self, ads_dict):
        self.ads = ads_dict
        self.status_code = None

        self.create_ad_post_json()

    def get_md5(self, image_file):
        image_md5 = hashlib.md5(open(image_file, 'rb').read()).hexdigest()
        return image_md5

    def make_image(self, image_file):
        try:
            img = Image.open(image_file)
            ad_image_md5 = self.get_md5(image_file)
            
            if img.format.lower() == 'webp':
                # new image name
                ad_image_filename = ad_image_md5 + '.jpg'
                img.save(Spidey.workdir + 'images/' + ad_image_filename)
            else:
                ad_image_filename = ad_image_md5 + '.' + img.format
                img.save(Spidey.workdir + 'images/' + ad_image_filename)

            logging.info('Make Image success!')

            return ad_image_filename
        except Exception as e:
            logging.info('make')
            logging.info(str(e.args))

    def process_image(self, image_file_path):
        img_dict = {}
        img_dict['image_filename'] = self.make_image(image_file_path)

        folder1 = str(random.randint(0, 100))
        folder2 = str(random.randint(0, 100))

        folder = '/'.join([folder1, folder2])

        img_dict['image_folder1'] = folder1
        img_dict['image_folder2'] = folder2
        img_dict['image_folder'] = folder

        return img_dict

    def s3_upload(self, file_source, file_destination):
        logging.info('s3 upload: %s, %s' %(file_source, file_destination))
        try:
            s3 = boto3.resource(
                    's3', 
                    aws_access_key_id = config.S3_ACCESS_KEY, 
                    aws_secret_access_key = config.S3_SECRET_KEY
                )
            # s3.Bucket(config.S3_BUCKET).upload_file(file_source, file_destination)

            logging.info('Uploading to s3 bucket started!')
            s3.meta.client.upload_file(
                file_source,
                config.S3_BUCKET,
                file_destination
            )
            logging.info('Uploading to s3 bucket completed!')
            
            return True
        except Exception as e:
            logging.info('exception in s3 upload: %s' % str(e.args))
            return None
    
    def cnc_post(self, cnc_ads_data):
        headers = {
                    'Content-type': 'application/json', 
                    'Accept': 'text/plain'
                }
        response = requests.post(
                        url = Ad.cnc_url,
                        headers = headers, 
                        data = cnc_ads_data
                )

        return response

    def create_ad_post_json(self):
        for i, item in enumerate(self.ads):
            try:
                ads_dict = self.ads[item]

                missing_data_field = {}
                missing_data = False
                cnc_ads_data = {}
                logging_info = {}

                cnc_ads_data['network_id'] = ads_dict['network_id']
                cnc_ads_data['publisher_id'] = ads_dict['publisher_id']
                cnc_ads_data['device_id'] = ads_dict['device_id']
                cnc_ads_data['country_id'] = ads_dict['country_id']

                if 'ad_line_1' in ads_dict:
                    cnc_ads_data['ad_line_1'] = ads_dict['ad_line_1'].strip()
                else:
                    missing_data = True
                    missing_data_field['missing_data_field'] = 'ad_line_1'

                if 'lp_url' in ads_dict:
                    cnc_ads_data['lp_url'] = ads_dict['lp_url'].strip()
                else:
                    missing_data = True
                    missing_data_field['missing_data_field'] = 'lp_url'
                
                if 'ad_image_src' in ads_dict:
                    image_uuid = str(uuid.uuid4())
                    ad_image_temp_file = ''.join(['ad-image_', image_uuid])
                    
                    # write image to spidey workdir
                    r = requests.get(ads_dict['ad_image_src'])
                    f = open(ad_image_temp_file, 'wb')
                    f.write(r.content)
                    f.close()

                    # move file to Spidey.workdir/images directory
                    new_image_path = '/'.join([Spidey.workdir, 'images/', ad_image_temp_file])
                    os.rename(ad_image_temp_file, new_image_path)

                    logging.info('Images moved from workdir to Spidey.workdir/images')

                    image_dict = self.process_image(new_image_path)

                    ad_image_file_path = Spidey.workdir + 'images/' + image_dict.get('image_filename')
                    ad_image_folder1 = image_dict.get('image_folder1')
                    ad_image_folder2 = image_dict.get('image_folder2')

                    cnc_ads_data['ad_image_filename'] = image_dict.get('image_filename')
                    cnc_ads_data['ad_image_folder'] = image_dict.get('image_folder')

                    s3_desination_path = 'assets/ad/' + image_dict.get('image_folder1') + '/' + image_dict.get('image_folder2') + '/' + image_dict.get('image_filename')
                    cnc_ads_data['s3_desination_path'] = s3_desination_path

                    # logging.info('s3 upload for ad img src started!')
                    # s3_upload_status = self.s3_upload(ad_image_file_path, s3_desination_path)

                    # if s3_upload_status:
                    #     logging.info('s3 upload for ad img src completed!')
                    #     # os.remove(ad_image_file_path)
                    #     # os.remove(new_image_path)
                    # else:
                    #     #RAISE info
                    #     logging.info('info S3_IMAGE_UPLOAD')
                    #     missing_data = True
                    #     missing_data_field['missing_data_field'] = 'info S3_IMAGE_UPLOAD'

                if 'lp_screenshot_file' in ads_dict:
                    lp_screenshot_file = Spidey.workdir + 'scrs/' + ads_dict.get('lp_screenshot_file')
                    s3_desination_path = 'lp/' + ads_dict.get('lp_screenshot_file')

                    # logging.info('s3 upload for lp screenshot file started!')
                    # s3_upload_status = self.s3_upload(lp_screenshot_file, s3_desination_path)
                    
                    cnc_ads_data['lp_screenshot_file'] = ads_dict.get('lp_screenshot_file')

                    # if s3_upload_status:
                    #     try:
                    #         logging.info('s3 upload for lp screenshot file completed!')
                    #         # os.remove(lp_screenshot_file)
                    #     except Exception as e:
                    #         missing_data = True
                    #         missing_data_field['missing_data_field'] = 'info lp_screenshot_file'
                    #         logging.info(str(e.args))

                if 'lp_html_file' in ads_dict:
                    lp_html_file = Spidey.workdir + 'lp_html/' + ads_dict.get('lp_html_file')
                    s3_desination_path = 'lp/html/' + ads_dict.get('lp_html_file')

                    # logging.info('s3 upload for lp html file started!')
                    # s3_upload_status = self.s3_upload(lp_html_file, s3_desination_path)
                    # logging.info('s3 upload for lp html file completed!')

                    # os.remove(lp_html_file)

                    cnc_ads_data['lp_html_file'] = ads_dict.get('lp_html_file')

                if missing_data:
                    logging.info(missing_data_field)
                else:
                    logging.info('cnc post init: ')
                    response = self.cnc_post(json.dumps(cnc_ads_data))
                    logging.info('cnc response status: %s' %response.status_code)

                    if response:
                        #SAVE AD_JSON POST TO s3
                        try:
                            rand = str(random.randint(1,10000))
                            ads_post_filename = 'ad-' + str(cnc_ads_data['publisher_id']) + '-' + str(Spidey.run_uuid) + '-' + rand + '.json'
                            ads_post_filepath = Spidey.workdir + ads_post_filename
                            s3_desination_path = 'json/' + ads_post_filename

                            fw = open(ads_post_filepath, 'w')
                            fw.write(json.dumps(cnc_ads_data))
                            fw.close()

                            logging.info('Saving ad json post to s3:')
                            # s3_upload_status = self.s3_upload(ads_post_filepath, s3_desination_path)
                            # os.remove(ads_post_filepath)
                        except Exception as e:
                            logging.info('s3 AD_JSON- CHECK POST %s' % str(e.args))

            except Exception as e:
                logging.info('create')
                logging.info(str(e.args))