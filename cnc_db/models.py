
from settings import DB


class CncDbAds(DB.Document):
    network_id = DB.StringField(db_field = 'ni')
    publisher_id = DB.StringField(db_field = 'pi')
    device_id = DB.StringField(db_field = 'di')
    country_id = DB.StringField(db_field = 'ci')
    ad_img_src = DB.StringField(db_field = 'ais')
    ad_line_1 = DB.StringField(db_field = 'al1')
    lp_url = DB.StringField(db_field = 'lu')
    lp_upload_folder = DB.StringField(db_field = 'luf')
    lp_screenshot_file = DB.StringField(db_field = 'lsf')
    lp_html_file = DB.StringField(db_field = 'lhf')


class CncPubs(DB.Document):
    url = DB.StringField(db_field = 'u')
    country_id = DB.StringField(db_field = 'ci')
    proxy_port = DB.StringField(db_field = 'pp')
    user_agent = DB.StringField(db_field = 'ua')
    country_code = DB.StringField(db_field = 'cc')
    publisher_id = DB.StringField(db_field = 'pi')
    device_id = DB.StringField(db_field = 'di')
    status = DB.StringField(db_field = 's')