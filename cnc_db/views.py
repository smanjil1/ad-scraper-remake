
import json
from flask import (
    Blueprint, 
    request, 
    render_template,
    url_for
)

from cnc_db.models import CncDbAds, CncPubs
from cnc_db.utils.get_ads_data import get_data
from cnc_db.utils.config import Config

cnc_db_app = Blueprint('cnc_db_app', __name__)


@cnc_db_app.route('/', methods = ['GET'])
def index():
    page =  request.args.get('page', 1, type = int)
    ads = get_data(page, Config.POSTS_PER_PAGE, False)

    next_url = url_for('cnc_db_app.index', page = ads.next_num) if ads.has_next else None
    prev_url = url_for('cnc_db_app.index', page = ads.prev_num) if ads.has_prev else None

    return render_template(
        'index.html',
        ads = ads.items,
        prev_url = prev_url,
        next_url = next_url
    )


@cnc_db_app.route('/post-data', methods = ['POST'])
def cnc_db_post():
    req_data = request.json

    cnc_ads = CncDbAds(
        network_id =  req_data['network_id'],
        publisher_id = req_data['publisher_id'],
        device_id = req_data['device_id'],
        country_id = req_data['country_id'],
        ad_img_src = req_data['ad_image_filename'],
        ad_line_1 = req_data['ad_line_1'],
        lp_url = req_data['lp_url'],
        # lp_upload_folder = req_data['lp_upload_folder'],
        lp_screenshot_file = req_data['lp_screenshot_file'],
        lp_html_file = req_data['lp_html_file']
    )
    cnc_ads.save()

    return 'Success!', 201


@cnc_db_app.route('/publisher', methods = ['GET'])
def get_publisher():
    # pub = CncPubs.objects.first()
    pub = CncPubs.objects(status = 'unvisited').first()

    # change the pub status to visited once pulled
    pub.status = 'visited'
    pub.save()
    
    return pub.to_json()


@cnc_db_app.route('/addpubs', methods = ['POST'])
def insert_publishers():
    req_data = request.json

    for pubs in req_data:
        pub = CncPubs(
            url = req_data[pubs]['url'],
            country_id = req_data[pubs]['country_id'],
            proxy_port = req_data[pubs]['proxy_port'],
            user_agent = req_data[pubs]['user_agent'],
            country_code = req_data[pubs]['country_code'],
            publisher_id = req_data[pubs]['publisher_id'],
            device_id = req_data[pubs]['device_id']
        )
        pub.save()

    return 'Success', 201