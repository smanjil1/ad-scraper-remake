
from cnc_db.models import CncDbAds


def get_data(page, per_page, error_show):
    ads_data = CncDbAds.objects.paginate(
        page, 
        per_page, 
        error_show
    )
    return ads_data