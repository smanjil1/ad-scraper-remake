
from flask import Flask
from cnc_db.views import cnc_db_app
from settings import DB


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('settings.py')

    DB.init_app(app)

    app.register_blueprint(cnc_db_app)

    return app